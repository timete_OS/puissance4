Puissance 4
==========


### Comment jouer ?

```sh
docker build . -t puissance
xhost +
docker run -it -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix puissance
```

Notez que cela peut être un peu long.

Si tout fonctionne bien, il est possible de jouer au puissance4. Notez que la taille de l'image Docker est énorme parce que je n'ai pas réussi à faire fonctionner l'interface dans une image finale, et que l'image finale est donc l'image de compilation.

![ une game](assets/demo.gif)


### Algo utilisé

Il s'agit d'un simple Qlearning tabulaire.

Pour garder l'espace des états *"petit*", un état n'est décris que par un vecteur de taille 7.
Chaque coordonnée du vecteur encode *"le nombre maximal de jetons alignés si ou joue sur cette colonne"*.

Ainsi, si l'état est **[1, 3, 1, 1, 0, 0, 0]**, jouer dans la deuxième colonne remporte la partie.

### Entrainement

Le docker fourni n'est pas entrainé. Pour entrainer le modèle sur 1000 parties, il faut executer : 

```sh
docker run -it --entrypoint  ./target/release/puissance4 train puissance 1000
```

Même après un million de partie, le modèle est toujours complètement nul.