use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::render::{WindowCanvas, Texture};
use sdl2::image::{LoadTexture};
use sdl2::rect::{Rect};
// use sdl2::ttf::Font;
// use sdl2::video::WindowContext;
use std::time::Duration;
use std::env;

// extern crate sdl2_unifont;

// use sdl2_unifont::renderer::SurfaceRenderer;

// use sdl2::pixels::Color;

mod game;
mod agent;

use crate::agent::Agent;
use crate::game::{Case, Game};

fn render(canvas: &mut WindowCanvas, game: &Game, color: Color, arrow: i32, textures: &[Texture]) -> Result<(), String> {
    canvas.set_draw_color(color);
    canvas.clear();

    let mut status = 7;
        
    
    if !game.playing {
        match game.current_player {
            Case::Red => status = 5,
            Case::Yellow => status = 6,
            Case::Empty => status = 8
        }
    }

    canvas.copy(&textures[status], None, Rect::new(150, 675, 400, 100))?;


    let color_to_texture = |c:&Case| {
        match c {
            Case::Empty => 0,
            Case::Red => 1,
            Case::Yellow => 2,
        }
    };


    // let (width, height) = canvas.output_size()?;

    canvas.copy(&textures[4], Rect::new(0, 0, 50, 50), Rect::new(arrow * 100 + 25, 25, 26, 26))?;

    for i in 0..7 {
        for j in 0..6 {
            let x = i*100 + 25;
            let y = (5 - j)*100 + 75;
            canvas.copy(&textures[color_to_texture(&game.board[i][j])],
            Rect::new(0, 0, 26, 26),
            Rect::new(x as i32, y as i32, 26, 26))?;
        }
    }


    canvas.present();

    Ok(())
}




fn run_game() -> Result<(), String>  {

    let window_width: u32 = 700;
    let window_height = 800;

    
    let mut game = Game::new();
    let agent = Agent::init();


    let ttf_context = sdl2::ttf::init().map_err(|e| e.to_string())?;

    let sdl_context = sdl2::init()?;
    let video_subsystem = sdl_context.video()?;

    let window = video_subsystem.window("Puissance4", window_width, window_height)
        .position_centered()
        .build()
        .expect("could not initialize video subsystem");

    let mut canvas = window.into_canvas().build()
        .expect("could not make a canvas");

    let texture_creator = canvas.texture_creator();
    // bad : I don't want files just for balls
    // let t = texture_creator.create_texture(format, access, width, height)


    // Load a font
    let mut font = ttf_context.load_font("assets/BerkeleyMonoTrial-Regular.ttf", 128)?;
    font.set_style(sdl2::ttf::FontStyle::BOLD);


    let surface = [font.render("Red Player Wins")
                                    .blended(Color::RGB(255, 0, 0))
                                    .map_err(|e| e.to_string())?,
                                font.render("Yellow Player Wins")
                                    .blended(Color::RGB(255, 255, 0))
                                    .map_err(|e| e.to_string())?,
                                font.render("Playing...")
                                    .blended(Color::RGB(0, 0, 0))
                                    .map_err(|e| e.to_string())?,
                                font.render("Draw")
                                    .blended(Color::RGB(0, 0, 0))
                                    .map_err(|e| e.to_string())?,
                                font.render("Training")
                                    .blended(Color::RGB(0, 0, 0))
                                    .map_err(|e| e.to_string())?
    ];

    // let texture = texture_creator
    //     .create_texture_from_surface(&surface[0])
    //     .map_err(|e| e.to_string())?;
    
    let textures = [
        texture_creator.load_texture("assets/black.png")?,
        texture_creator.load_texture("assets/red.png")?,
        texture_creator.load_texture("assets/yellow.png")?,
        texture_creator.load_texture("assets/blue.png")?,
        texture_creator.load_texture("assets/arrow.png")?,
        texture_creator.create_texture_from_surface(&surface[0]).map_err(|e| e.to_string())?,
        texture_creator.create_texture_from_surface(&surface[1]).map_err(|e| e.to_string())?,
        texture_creator.create_texture_from_surface(&surface[2]).map_err(|e| e.to_string())?,
        texture_creator.create_texture_from_surface(&surface[3]).map_err(|e| e.to_string())?,
        texture_creator.create_texture_from_surface(&surface[4]).map_err(|e| e.to_string())?,
    ];

    let mut event_pump = sdl_context.event_pump()?;
    let mut arrow: usize = 2;
    let mut reward;
    'running: loop {
        // Handle events
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running;
                },
                Event::KeyDown {keycode: Some(Keycode::Left), .. } => {
                    if arrow == 0 {
                        arrow = 6;
                    } else {
                        arrow -= 1;
                    }
                }
                Event::KeyDown {keycode: Some(Keycode::Right), .. } => {
                    arrow = (arrow + 1)%7;
                }
                Event::KeyDown {keycode: Some(Keycode::Down), .. } => {
                    reward = game.play(arrow);
                    if reward != 0. {
                        let state = game.get_state();
                        let ai_move = agent.policy(state, game.actions());
                        game.play(ai_move);
                    }
                    // println!("move : {ai_move}");
                    // println!("reward : {reward}");
                    // println!("{:?}", game.current_player);
                    // println!("{:?}", game.get_state());
                }
                Event::KeyDown {keycode: Some(Keycode::Space), .. } => {
                    game = Game::new();
                    // println!("{:?}", game.current_player);
                    // println!("{:?}", game.get_state());
                }
                _ => {}
            }
        }

        // Render
        render(&mut canvas, &game, Color::RGB(95, 173, 86), arrow as i32, &textures)?;

        // Time management!
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }

    Ok(())
}


fn main() {
    let args: Vec<String> = env::args().collect();


    match args.len() {
        1 => {
            let _res = run_game();   
        },
        2 => {

        },
        3 => {
            let cmd = &args[1];
            let games: usize = match &args[2].parse::<usize>() {
                Ok(n) => *n,
                Err(_) => return
            };
            match &cmd[..] {
                "train" => {
                    let mut agent = Agent::init();
                    agent.train(games);
                    println!("Done : played {games} games, saved to memory.npy");        
                },
                _ => println!("unknown argument")
            }
        }
        _ => ()
    }


}