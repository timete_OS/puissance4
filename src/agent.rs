use ndarray::{Array2, ArrayView1};
use ndarray_rand::RandomExt;
use ndarray_rand::rand_distr::Uniform;
use ndarray_npy::{ReadNpyExt, WriteNpyExt};
use argminmax::ArgMinMax;
use rand::Rng;
use rand::rngs::ThreadRng;
use std::fs::File;
use std::io::BufWriter;

use crate::game::Game; 


pub struct Agent {
    q_table: Array2<f64>
}


impl Agent {

    pub fn init() -> Self {
        let base:usize = 6;
        let state_size = base.pow(7);
        let q: Array2<f64>;

        // let reader = File::open("memory.npy");
        // if reader.is_ok() {
        //     let q = Array2::<f64>::read_npy(reader.into_ok());
        //     if q.is_ok() {
        //         return Self {q_table : q.into_ok()}
        //     }
        // }
        let reader = File::open("memory.npy");
        match reader {
            Err(_e) => {
                println!("there is no memory file");
                q =  Array2::random((state_size, 7), Uniform::new(-10., 10.));
            }
            Ok(r) => match Array2::<f64>::read_npy(r) {
                Ok(qf) => {
                    println!("using memory file");
                    q = qf;
                },
                Err(_e) => {
                    println!("canno't read memory file");
                    q = Array2::random((state_size, 7), Uniform::new(-10., 10.));
                }
            }            
        }

        Self {
            q_table: q
            }
        }
    

    pub fn policy(&self, state: [usize; 7], actions: [f64; 7]) -> usize {
        let index = self.state_to_index(&state);
        let tmp = &self.q_table.row(index) + &ArrayView1::from(&actions);
        let (_min, max) =  tmp.argminmax();
        max
    }

    fn state_to_index(&self, state: &[usize; 7]) -> usize {
        let mut index = 0;
        let base: usize = 6;
        for i in 0..6 {
            index += state[i] * base.pow(i as u32);
        }
        index
    }

    fn train_policy(&self, game: &mut Game, rng: &mut ThreadRng) -> (usize, f64, usize) {
        let actions = game.actions();
        let state_index = self.state_to_index(&game.get_state());

        let tmp = &self.q_table.row(state_index) + &ArrayView1::from(&actions);
        let (_min, mut new_max) =  tmp.argminmax();

        // random move with proba epsilon/255
        let epsilon: u8 = (*rng).gen();
        if epsilon < 32 {
            new_max = (epsilon as usize)%7;
        }
        let reward = game.play(new_max);
        (state_index, reward, new_max)
    }


    pub fn train(&mut self, training_game: usize) {
        let alpha = 1.;
        let gamma = 0.6;
        let mut rng = rand::thread_rng();
        
        for round in 0..training_game {
            let progress = round*100 / training_game;
            print!("Progress : {progress} %\r");

            let mut game = Game::new();

            // player 1 plays
            let (mut player_1_index, mut player_1_reward, mut player_1_max) = self.train_policy(&mut game, &mut rng);

            // player two plays
            let (mut player_2_index,mut player_2_reward,mut player_2_max) = self.train_policy(&mut game, &mut rng);
    

            while game.playing {
                // player 1 plays
                let (player_1_new_index, player_1_new_reward, player_1_new_max) = self.train_policy(&mut game, &mut rng);

                // update one rewards
                let tmp = self.q_table[[player_1_index, player_1_max]];
                self.q_table[[player_1_index, player_1_max]] += alpha * (player_1_reward
                                                            + gamma * (self.q_table.row(player_1_new_index)[player_1_new_max] - tmp));
                
                player_1_index = player_1_new_index;
                player_1_reward = player_1_new_reward;
                player_1_max = player_1_new_max;

                // player 2 plays
                let (player_2_new_index, player_2_new_reward, player_2_new_max) = self.train_policy(&mut game, &mut rng);

                // update two rewards
                let tmp = self.q_table[[player_2_index, player_2_max]];
                self.q_table[[player_2_index, player_2_max]] += alpha * (player_2_reward
                                                            + gamma * (self.q_table.row(player_2_new_index)[player_2_new_max] - tmp));
                
                player_2_index = player_2_new_index;
                player_2_reward = player_2_new_reward;
                player_2_max = player_2_new_max;
    
            }
            // need to update rewards once the game is finished 
            // otherwise the model don't have the end reward

            // update one rewards
            let mut tmp = self.q_table[[player_1_index, player_1_max]];
            self.q_table[[player_1_index, player_1_max]] += alpha * (player_1_reward
                                                            + gamma * (self.q_table.row(player_1_index)[player_1_max] - tmp));
            // update two rewards
            tmp = self.q_table[[player_2_index, player_2_max]];
            self.q_table[[player_2_index, player_2_max]] += alpha * (player_2_reward
                                                            + gamma * (self.q_table.row(player_2_index)[player_2_max] - tmp));

        }

        // write to file
        let writer = BufWriter::new(File::create("memory.npy").expect("can't create file"));
        self.q_table.write_npy(writer).expect("memory not saved !!!!");
        
    }

}
