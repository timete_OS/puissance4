use std::cmp::{min, max};


#[derive(Clone, PartialEq, Debug)]
pub enum Case {
    Empty,
    Red,
    Yellow
}


pub struct Game {
    pub board: Vec<Vec<Case>>,
    state: [usize; 7],
    pub playing: bool,
    free_slots: usize,
    pub current_player: Case
}

impl Game {
    pub fn new() -> Self {
        Self {
            board: vec![vec![Case::Empty; 6]; 7],
            playing: true,
            state: [0; 7],
            current_player: Case::Red,
            free_slots: 6*7
        }
    }

    pub fn play(&mut self, column: usize) -> f64 {
        if !self.playing {
            return  0.;
        }
        let i = min(column, 6);
        let mut j = 0;

        while j < 6 && self.board[i][j] != Case::Empty {
            j += 1;
        }
        if j < 6 {
            self.board[i][j] = self.current_player.clone();
            self.free_slots -= 1;
        } else {
            return 0.
        }

        if self.check_for_win(column) || self.free_slots == 0{
            self.update_state();
            self.playing = false;
            if self.free_slots == 0 {
                self.current_player = Case::Empty;
            }
            return 1234. * self.free_slots as f64
        }

        //  switch turn
        match self.current_player {
            Case::Red => self.current_player = Case::Yellow,
            Case::Yellow => self.current_player = Case::Red,
            _ => ()
        }
        self.update_state();

        -1.
    }

    fn count_colors(&self, i: usize, j: usize) -> usize {
        let mut line = 0;
        let mut diag_r = 0;
        let mut diag_l = 0;
        let mut b = true;
        // line
        for h in (0..i).rev() {
            b = b && self.board[h][j] == self.current_player;
            if b {
                line += 1;
            }
        }
        b = true;
        for h in (i+1)..7 {
            b = b && self.board[h][j] == self.current_player;
            if b {
                line += 1;
            }
        }
        // if j == 0 {
        //     return  line;
        // }
        // diag right
        b = true;
        for h in 0..min(6 - i, j) {
            b = b && self.board[i + h + 1][j - h - 1] == self.current_player;
            if b {
                diag_r += 1;
            }
        }
        b = true;
        for h in 0..min(i, 5 - j) {
            b = b && self.board[i - h - 1][j + h + 1] == self.current_player;
            if b {
                diag_r += 1;
            }
        }
        // diag left
        b = true;
        for h in 0..min(i, j) {
            b = b && self.board[i - h - 1][j - h - 1] == self.current_player;
            if b {
                diag_l += 1;
            }
        }
        b = true;
        for h in 0..min(6 - i, 5 - j) {
            b = b && self.board[i + h + 1][j + h + 1] == self.current_player;
            if b {
                diag_l += 1;
            }
        }

        max(line, max(diag_l, diag_r))
    }

    fn update_state(&mut self) -> [usize; 7] {
        let mut state = [0; 7];
        for i in 0..7 {
            let mut j = 0;
            let mut count_col = 0;
            while (j < 6) && self.board[i][j] != Case::Empty {
                if self.board[i][j] == self.current_player {
                    count_col += 1;
                } else {
                    count_col = 0;
                }
                j += 1;
            }
            if j == 6 {
                state[i] = 0;
            } else {
                state[i] = max(count_col, self.count_colors(i, j));
            }          
        }
        self.state = state;
        state
    }

    pub fn actions(&self) -> [f64; 7] {
        let mut vec = [f64::NEG_INFINITY; 7];
        // let almost_min:f64 = 10.;
        // let mut vec = [almost_min.powi(-10); 7];
        for i in 0..7 {
            if self.board[i][5] == Case::Empty {
                vec[i] = 0.;
            }
        }
        vec
    }

    pub fn get_state(&self) -> [usize; 7] {
        self.state
    }

    fn check_for_win(&self, i: usize) -> bool {
       self.state[i] >= 3
    }
}
