FROM rust as builder
WORKDIR /usr/src/puissance4
COPY . .

RUN apt update
RUN apt install build-essential
RUN apt install -y libsdl2-dev
RUN apt install -y libsdl2-image-dev
RUN apt install -y libsdl2-ttf-dev
RUN apt install -y cmake
# need of cmake or gcc is installed


RUN rustup install nightly
RUN rustup default nightly
RUN cargo build --release


ENTRYPOINT ["./target/release/puissance4"]

# FROM debian:bullseye-slim

# WORKDIR /usr/local/puissance4

# RUN apt-get update &&  apt-get install -y libsdl2-2.0-0  &&  apt-get install -y  libsdl2-image-2.0-0 &&  apt-get install -y libsdl2-ttf-2.0-0 && rm -rf /var/lib/apt/lists/*
# COPY --from=builder /usr/src/puissance4/target/release/puissance4  .
# COPY --from=builder /usr/src/puissance4/memory.npy  .


# ENTRYPOINT ["./puissance4"]

# docker run -it -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --entrypoint ./puissance4 puissance:latest

# docker build . -t puisance